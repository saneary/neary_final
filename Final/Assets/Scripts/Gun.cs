﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{

    public GameObject claw;
    public bool isShooting;
    public Animator minerAnimator;
    public Claw clawScript;
    public bool hasObject;

    void Awake()
    {
        hasObject = false;
    }
        void Update()
    {
        if (Input.GetButtonDown("Fire1") && !isShooting)
        {
            if (hasObject == false)
            {
                LaunchClaw();
            }
            else if (hasObject == true)
            {
                Throw();
            }
        }

    }

    void LaunchClaw()
    {
        print("clawlaunched");
        isShooting = true;
        minerAnimator.speed = 0;
        RaycastHit hit;
        Vector3 down = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position, down, out hit, 100))
        {
            print("raycast"+hit.point);
            claw.SetActive(true);
            
            clawScript.ClawTarget(hit.point);
        }
    }

    void Throw()
    {
        print("throw");
    }

    public void CollectedObject()
    {
        isShooting = false;
        minerAnimator.speed = 1;
        hasObject = true;
    }

}